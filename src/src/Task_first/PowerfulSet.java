package Task_first;
// Реализовать класс PowerfulSet, в котором должны быть следующие методы:
//      a. public <T> Set<T> intersection(Set<T> set1, Set<T> set2) — возвращает
//    пересечение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
//  Вернуть {1, 2}
//      b.public<T> Set<T> union(Set<T> set1,Set<T> set2) — возвращает
//    объединение двух наборов.Пример:set1={1,2,3},set2={0,1,2,4}.
//  Вернуть{0,1,2,3,4}
// c.public<T> Set<T> relativeComplement(Set<T> set1,Set<T> set2) —
// возвращает элементы первого набора без тех,которые находятся также
// и во втором наборе.Пример:set1={1,2,3},set2={0,1,2,4}.Вернуть{3}

import java.util.HashSet;
import java.util.Set;

public class PowerfulSet {
    public static void main(String[] args) {
        Set<Integer> set1 = Set.of(1, 2, 3);
        Set<Integer> set2 = Set.of(0, 1, 2, 4);

        Set<Integer> common = intersection(set1, set2);
        System.out.println(common);
        common = union(set1, set2);
        System.out.println(common);
        common = relativeComplement(set1, set2);
        System.out.println(common);
    }


    //  возвращает
    //  пересечение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
    //  Вернуть {1, 2}
    public static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> common = new HashSet<>(set1);
        common.retainAll(set2);
        return (Set<T>) common;

    }
    //  объединение двух наборов.Пример:set1={1,2,3},set2={0,1,2,4}.
    //  Вернуть{0,1,2,3,4}

    public static <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> set = new HashSet<>();
        set.addAll(set1);
        set.addAll(set2);
        return set;
    }

    //возвращает элементы первого набора без тех, которые находятся также
    //и во втором наборе. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {3}
    public static <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        Set<T> set = new HashSet<>();
        set.addAll(set1);
        set.removeAll(set2);
        return set;

    }

}

