package Task_first;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//В некоторой организации хранятся документы (см. класс Document). Сейчас все
//документы лежат в ArrayList, из-за чего поиск по id документа выполняется
//неэффективно. Для оптимизации поиска по id, необходимо помочь сотрудникам
//перевести хранение документов из ArrayList в HashMap.
public class Task4 {
    private static class Document {
        public int id;
        public String name;
        public int pageCount;

        public Document(int id, String name, int pageCount) {
            this.id = id;
            this.name = name;
            this.pageCount = pageCount;
        }

        @Override
        public String toString() {
            return "Document{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", pageCount=" + pageCount +
                    '}';
        }
    }

    public static void main(String[] args) {
        Document doc1 = new Document(1, "First", 10);
        Document doc2 = new Document(2, "Second", 30);
        Document doc3 = new Document(3, "Third", 20);
        Document doc4 = new Document(4, "Four", 50);
        final List<Document> documents = List.of(doc1, doc2, doc3, doc4);
        var catalog = organizeDocuments(documents);
        System.out.println(catalog.get(4));
    }

    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> paper = new HashMap<>();
        for (Document document : documents) {
            paper.put(document.id, document);

        }
        return paper;
    }
}
