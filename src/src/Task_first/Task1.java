package Task_first;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Task1 {

    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("first");
        list.add("first");
        list.add("second");
        Set<String> unique = new HashSet<>(list);
        System.out.println(unique);

    }
}
