package Task_first;
//Реализовать метод, который принимает массив words и целое положительное число k.
//        Необходимо вернуть k наиболее часто встречающихся слов..
//        Результирующий массив должен быть отсортирован по убыванию частоты
//        встречаемого слова. В случае одинакового количества частоты для слов, то
//        отсортировать и выводить их по убыванию в лексикографическом порядке.

import java.util.*;

//words = ["the","day","is","sunny","the","the","the",
//        "sunny","is","is","day"]
//        k = 4
//output: ["the","is","sunny","day"]
public class AdditionalTask {
    public static void main(String[] args) {
        String[] words = {"the", "sunny", "day", "is", "the", "the",
                "the", "sunny", "is", "is", "day"};
        int k = 4;

        System.out.println(largeNumberWords(words, k));
    }

    public static String largeNumberWords(String[] words, int k) {
        Map<String, Integer> map = new HashMap<>();

        for (String str : words) {
            int count = 1;
            if (!map.containsKey(str))
                map.put(str, count);
            else
                map.put(str, map.get(str) + 1);
        }
        List<String> key = new ArrayList<>(map.keySet());
        List<Integer> value = new ArrayList<>(map.values());

        String[] result = new String[k];

        for (int i = 0; i < k; i++) {
            int maxValue = Integer.MIN_VALUE;
            for (Integer x : value) {
                if (x > maxValue)
                    maxValue = x;
            }
            result[i] = key.get(value.indexOf(maxValue));
            key.remove(value.indexOf(maxValue));
            value.remove((Integer) maxValue);
        }

        return Arrays.toString(result);
    }

}
